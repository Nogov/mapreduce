﻿using MapReduce.ObjectOriented;
using System;
using System.Collections.Generic;

namespace MapReduceExperiments
{
	public class SumCounting
	{
		public class Map : Map<int, int>
		{
			public Map(IEnumerable<int> data)
				: base(data, false) { }

			protected override int Mapping(int key) => key;

			protected override int Combining(int accumulator, int value)
			{
				throw new NotImplementedException();
			}
		}

		public class Reduce : Reduce<int, int>
		{
			public Reduce(Dictionary<int, List<int>> data)
				: base(data) { }

			protected override int Reducing(int accumulator, int value) => accumulator + value;
		}
	}
}