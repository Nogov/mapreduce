﻿using MapReduce.ObjectOriented;
using System.Collections.Generic;

namespace MapReduceExperiments
{
	public class LengthCounting
	{
		public class Map : Map<string, int>
		{
			public Map(IEnumerable<string> data)
				: base(data, true) { }

			protected override int Combining(int accumulator, int value) => accumulator + value;
			protected override int Mapping(string key) => key.Length;
		}
	}
}
