﻿namespace System.Collections.Generic
{
	public static class FunctionalExtensions
	{
		/// <summary>
		/// Folds a <paramref name="set"/> by a given folding function.
		/// </summary>
		/// <typeparam name="TElement">Type of elements.</typeparam>
		/// <typeparam name="TResult">Type of result.</typeparam>
		/// <param name="set">Set of elements.</param>
		/// <param name="func">Folding function.</param>
		/// <returns>Folding result.</returns>
		public static TResult Fold<TElement, TResult>(
			this IEnumerable<TElement> set,
			Func<TResult, TElement, TResult> func)
		{
			var result = default(TResult);
			foreach (var element in set)
				result = func(result, element);
			return result;
		}
	}
}