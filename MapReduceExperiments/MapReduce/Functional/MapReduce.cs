namespace MapReduce.Functional
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;

	/// <summary>
	/// A functional approach to perform MapReduce pattern over enumerables.
	/// </summary>
	public static class MapReduce
	{
		/// <summary>
		/// Performs as-is mapping to each element of <paramref name="source" />.
		/// </summary>
		/// <typeparam name="TData">Type of data.</typeparam>
		/// <param name="source">Source.</param>
		/// <returns>Combiner with mapped data.</returns>
		public static Combiner<TData, TData> MapAsIs<TData>(
			this IEnumerable<TData> source)
			=> Map(source, x => x);

		/// <summary>
		/// Performs mapping to each element of <paramref name="source" />.
		/// </summary>
		/// <typeparam name="TKey">Type of key.</typeparam>
		/// <typeparam name="TValue">Type of values.</typeparam>
		/// <param name="source">Source.</param>
		/// <param name="mapper">Mapping function.</param>
		/// <returns>Combiner with mapped data.</returns>
		public static Combiner<TKey, TValue> Map<TKey, TValue>(
			this IEnumerable<TKey> source,
			Func<TKey, TValue> mapper)
		{
			var combiner = new Combiner<TKey, TValue>();
			foreach (var item in source)
				combiner.Emit(item, mapper(item));
			return combiner;
		}

		/// <summary>
		/// Reduces values of current <paramref name="combiner" />.
		/// <seealso cref="Combiner{TKey, TValue}"/>.
		/// </summary>
		/// <typeparam name="TKey">Type of key.</typeparam>
		/// <typeparam name="TValue">Type of values.</typeparam>
		/// <typeparam name="TResult">Type of result.</typeparam>
		/// <param name="combiner">Combiner with mapped data.</param>
		/// <param name="reducer">Reducing function.</param>
		/// <returns>Results.</returns>
		public static IEnumerable<TResult> Reduce<TKey, TValue, TResult>(
			this Combiner<TKey, TValue> combiner,
			Func<TResult, TValue, TResult> reducer)
		{
			var tasks = new Task<TResult>[combiner.Keys.Count()];
			var i = 0;
			foreach (var key in combiner.Keys)
				tasks[i++] = Task.Run(() => combiner[key].Fold(reducer));
			Task.WaitAll(tasks);
			return tasks.Select(t => t.Result);
		}

		/// <summary>
		/// Merges reducers output.
		/// <seealso cref="Reduce{TKey, TValue, TResult}(Combiner{TKey, TValue}, Func{TResult, TValue, TResult})"/>.
		/// </summary>
		/// <typeparam name="TResult">Type of result.</typeparam>
		/// <param name="reducersOutput">Reducers output.</param>
		/// <param name="merger">Merging function.</param>
		/// <returns>Merged output.</returns>
		public static TResult Merge<TResult>(
			this IEnumerable<TResult> reducersOutput,
			Func<TResult, TResult, TResult> merger)
			   => reducersOutput.Aggregate(merger);

		/// <summary>
		/// Provides horizontal mapping combining.
		/// </summary>
		/// <typeparam name="TKey">Type of key.</typeparam>
		/// <typeparam name="TValue">Type of value.</typeparam>
		public sealed class Combiner<TKey, TValue>
		{
			private Dictionary<TKey, List<TValue>> _mapping { get; }
				= new Dictionary<TKey, List<TValue>>();

			/// <summary>
			/// Keys.
			/// </summary>
			public IEnumerable<TKey> Keys => _mapping.Keys;

			/// <summary>
			/// Gets mapped data stored by given <paramref name="key"/>.
			/// </summary>
			/// <param name="key">Key.</param>
			/// <returns>Mapped data.</returns>
			public IEnumerable<TValue> this[TKey key] => _mapping[key];

			/// <summary>
			/// Combines and stores <paramref name="value"/> by given <paramref name="key"/>.
			/// </summary>
			/// <param name="key">Key.</param>
			/// <param name="value">Value.</param>
			public void Emit(TKey key, TValue value)
			{
				if (_mapping.ContainsKey(key))
					_mapping[key].Add(value);
				else
					_mapping.Add(key, new List<TValue> { value });
			}
		}
	}
}