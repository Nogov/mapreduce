/// <summary>
/// An object-oriented approach to perform MapReduce pattern.
/// </summary>
namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;

	/// <summary>
	/// A representation of Map part of MapReduce pattern.
	/// </summary>
	/// <typeparam name="TKey">Type of key.</typeparam>
	/// <typeparam name="TValue">Type of value.</typeparam>
	public abstract partial class Map<TKey, TValue> : IMap<TKey, TValue>
	{
		/// <summary>
		/// Indicates whether to use combiner.
		/// </summary>
		private readonly bool _useCombiner;

		/// <summary>
		/// Data set.
		/// </summary>
		protected IEnumerable<TKey> _data { get; }

		/// <summary>
		/// Initializes a new instance of a <see cref="Map{TKey, TValue}"/>. 
		/// </summary>
		/// <param name="data">Data set.</param>
		/// <param name="useCombiner">Use combining phase.</param>
		protected Map(IEnumerable<TKey> data, bool useCombiner = false)
		{
			_data = data;
			_useCombiner = useCombiner;
		}

		/// <summary>
		/// Applies mapping to data set (<see cref="_data"/>).
		/// </summary>
		/// <remarks>Uses <see cref="Emitter"/>.</remarks>
		/// <returns>Mapped data.</returns>
		private IEnumerable<KeyValuePair<TKey, TValue>> ApplyMapping()
		{
			var emitter = new Emitter();
			foreach (var item in _data)
				emitter.Emit(item, Mapping(item));
			return emitter.Entries;
		}

		/// <summary>
		/// Combines <paramref name="data"/>.
		/// </summary>
		/// <param name="data">Data set.</param>
		/// <returns>Combined data.</returns>
		private Dictionary<TKey, TValue> Combine(IEnumerable<KeyValuePair<TKey, TValue>> data)
		{
			var combined = new Dictionary<TKey, TValue>();
			foreach (var entry in data)
			{
				var key = entry.Key;
				if (combined.ContainsKey(key))
				{
					var combinedValue = combined[key];
					combined[key] = Combining(combinedValue, entry.Value);
				}
				else
					combined.Add(key, entry.Value);
			}
			return combined;
		}

		/// <summary>
		/// Shuffles <paramref name="data"/>.
		/// </summary>
		/// <param name="data">Data set.</param>
		/// <remarks>Uses <see cref="Shuffler"/></remarks>
		/// <returns>Shuffled data.</returns>
		private Dictionary<TKey, List<TValue>> Shuffle(IEnumerable<KeyValuePair<TKey, TValue>> data)
		{
			var shuffler = new Shuffler(data);
			return shuffler.Execute();
		}

		/// <summary>
		/// Mapping which applies to an each entry of the given data set (<see cref="_data"/>).
		/// </summary>
		/// <param name="key">Key.</param>
		/// <returns>Value.</returns>
		protected abstract TValue Mapping(TKey key);

		/// <summary>
		/// Combining which applies to an each entry of the given data set (<see cref="_data"/>).
		/// It accumulates interim value into <paramref name="accumulator"/>.
		/// </summary>
		/// <param name="accumulator">Accumulator.</param>
		/// <param name="value">Value.</param>
		/// <returns>Combined value.</returns>
		protected abstract TValue Combining(TValue accumulator, TValue value);

		/// <summary>
		/// Executes mapping.
		/// </summary>
		/// <remarks>The entry point.</remarks>
		/// <returns>Mapped data.</returns>
		public Dictionary<TKey, List<TValue>> Execute()
		{
			var mapped = ApplyMapping();
			var data = !_useCombiner ? mapped : Combine(mapped);
			return Shuffle(data);
		}
	}
}