﻿namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;
	
	public interface IReduce<TKey, TValue>
	{
		Dictionary<TKey, TValue> Execute();
	}
}