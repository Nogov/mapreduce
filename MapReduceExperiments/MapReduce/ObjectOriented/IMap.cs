﻿namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;

	public interface IMap<TKey, TValue>
	{
		Dictionary<TKey, List<TValue>> Execute();
	}
}