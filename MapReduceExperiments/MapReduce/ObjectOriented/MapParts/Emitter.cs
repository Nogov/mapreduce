﻿namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;

	public partial class Map<TKey, TValue> 
	{
		/// <summary>
		/// Emitter.
		/// </summary>
		/// <typeparam name="TKey">Type of key.</typeparam>
		/// <typeparam name="TValue">Type of value.</typeparam>
		private sealed class Emitter
		{
			/// <summary>
			/// List of entries.
			/// </summary>
			public List<KeyValuePair<TKey, TValue>> Entries { get; }
				= new List<KeyValuePair<TKey, TValue>>();

			/// <summary>
			/// Combines and stores <paramref name="value"/> by given <paramref name="key"/>.
			/// </summary>
			/// <param name="key">Key.</param>
			/// <param name="value">Value.</param>
			public void Emit(TKey key, TValue value) 
				=> Entries.Add(new KeyValuePair<TKey, TValue>(key, value));
		}
	}
}
