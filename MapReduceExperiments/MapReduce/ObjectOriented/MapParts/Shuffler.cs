﻿namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;

	public partial class Map<TKey, TValue>
	{
		/// <summary>
		/// A representation of Shuffling part of MapReduce pattern.
		/// </summary>
		/// <typeparam name="TKey">Type of key.</typeparam>
		/// <typeparam name="TValue">Type of value.</typeparam>
		private sealed class Shuffler
		{
			private readonly IEnumerable<KeyValuePair<TKey, TValue>> _emitted;

			public Shuffler(IEnumerable<KeyValuePair<TKey, TValue>> emitted)
			{
				_emitted = emitted;
			}

			/// <summary>
			/// Combines and stores <paramref name="value"/> by given <paramref name="key"/>.
			/// </summary>
			/// <param name="key">Key.</param>
			/// <param name="value">Value.</param>
			public Dictionary<TKey, List<TValue>> Execute()
			{
				var shuffled = new Dictionary<TKey, List<TValue>>();
				foreach (var entry in _emitted)
				{
					var key = entry.Key;
					var value = entry.Value;
					if (shuffled.ContainsKey(key))
						shuffled[key].Add(value);
					else
						shuffled.Add(key, new List<TValue> { value });
				}
				return shuffled;
			}
		}
	}
}
