﻿/// <summary>
/// An object-oriented approach to perform MapReduce pattern.
/// </summary>
namespace MapReduce.ObjectOriented
{
	using System.Collections.Generic;

	/// <summary>
	/// A representation of Reduce part of MapReduce pattern.
	/// </summary>
	/// <typeparam name="TKey">Type of key.</typeparam>
	/// <typeparam name="TValue">Type of value.</typeparam>
	public abstract class Reduce<TKey, TValue> : IReduce<TKey, TValue>
	{
		/// <summary>
		/// Data set.
		/// </summary>
		private readonly Dictionary<TKey, List<TValue>> _data;

		/// <summary>
		/// Initializes a new instance of a <see cref="Reduce{TKey, TValue}"/>. 
		/// </summary>
		/// <param name="data">Data set.</param>
		protected Reduce(Dictionary<TKey, List<TValue>> data)
		{
			_data = data;
		}

		/// <summary>
		/// Reducing which applies to an each entry of the given data set (<see cref="_data"/>).
		/// It accumulates interim value into <paramref name="accumulator"/>.
		/// </summary>
		/// <param name="accumulator">Accumulator.</param>
		/// <param name="value">Value.</param>
		/// <returns>Reduced value.</returns>
		protected abstract TValue Reducing(TValue accumulator, TValue value);

		/// <summary>
		/// Executes reducing.
		/// </summary>
		/// <remarks>The entry point.</remarks>
		/// <returns>Reduced data.</returns>
		public Dictionary<TKey, TValue> Execute()
		{
			var reduced = new Dictionary<TKey, TValue>();
			foreach (var key in _data.Keys)
			{
				if (!reduced.ContainsKey(key))
					reduced.Add(key, default(TValue));
				foreach (var value in _data[key])
				{
					var reducedValue = reduced[key];
					reduced[key] = Reducing(reducedValue, value);
				}
			}
			return reduced;
		}
	}
}