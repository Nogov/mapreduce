﻿using MapReduce.Functional;
using MapReduceExperiments.Tools;
using System;
using System.Linq;

namespace MapReduceExperiments
{
	public static class Program
	{
		public static void Main()
		{
			var collection = new int[] { 4, 4, 4, 4, 4, 1, 1, 5, 12, 2, 3, 3, 4, 1 };

			using (new DisposableWatch("Object-oriented approach"))
				OOApproach(collection);

			using (new DisposableWatch("Functional approach"))
				FunctionalApproach(collection);

			Console.ReadKey();
		}

		private static void FunctionalApproach(int[] dataSet)
		{
			Func<int, int> mapper = x => x;
			Func<int, int, int> reducer = (acc, v) => acc + v;
			Func<int, int, int> merger = (x, y) => x + y;
			var result = dataSet.Map(mapper).Reduce(reducer).Merge(merger);
			Console.WriteLine(result);
		}

		private static void OOApproach(int[] dataSet)
		{
			var mapResult = new SumCounting.Map(dataSet).Execute();
			var reduceResult = new SumCounting.Reduce(mapResult).Execute();
			var result = reduceResult.Values.Sum();
			Console.WriteLine(result);
		}
	}
}