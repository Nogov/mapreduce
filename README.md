# README #

Implementing the MapReduce pattern in two ways: functional and object-oriented over IEnumerable.

I was following the image below.


![hadoop-F03_reference.jpg](https://bitbucket.org/repo/Eg69x9n/images/743098715-hadoop-F03_reference.jpg)

This code is under development, so, it is a draft.

P.S. Just for educational purposes.